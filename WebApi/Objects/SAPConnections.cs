﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PruebaRendimiento02_web.Objects
{
    public static class SAPConnections
    {
        /// <summary>
        /// Checks if an user is registered on SAP, in the UDO of mobile users
        /// </summary>
        /// <param name="_sapUsername"></param>
        /// <param name="_sapPassword"></param>
        /// <param name="_server"></param>
        /// <param name="_companyDB"></param>
        /// <param name="_user"></param>
        /// <param name="_password"></param>
        /// <param name="_dbServerType"></param>
        /// <returns>0 = Not registered, 1 = Registered, -1 connection error</returns>
        private static int CheckUserOnSAP (string _sapUsername, string _sapPassword,
                                           string _licenseServer, string _server, string _companyDB,
                                           string _user, string _password,
                                           SAPbobsCOM.BoDataServerTypes _dbServerType,
                                           out string connectionGroup, out CompanyConnection companyConnection)
        {
            int _isSAPUser = 0;
            SAPbobsCOM.CompanyService oCompanyService;
            SAPbobsCOM.GeneralService oGeneralService;
            SAPbobsCOM.GeneralData oGeneralData;
            SAPbobsCOM.GeneralDataParams oGeneralDataParams;
            

            companyConnection = new CompanyConnection(_licenseServer, _server, _companyDB, _user, _password, _dbServerType);
           // ((SAPbobsCOM.Company)companyConnection).LicenseServer = "192.168.1.155:30000";
            int _connectionError = companyConnection.Connect();

            if (_connectionError == 0)
            {
                try
                {
                    oCompanyService = companyConnection.GetCompany(false).GetCompanyService();
                    oGeneralService = oCompanyService.GetGeneralService("DOIUSR");
                    oGeneralDataParams = oGeneralService.GetDataInterface(SAPbobsCOM.GeneralServiceDataInterfaces.gsGeneralDataParams);

                    //oGeneralDataParams.GetProperty("name")
                    oGeneralDataParams.SetProperty("Code", _sapUsername);
                    oGeneralData = oGeneralService.GetByParams(oGeneralDataParams);

                    if (oGeneralData.GetProperty("U_Password") == _sapPassword)
                    {
                        _isSAPUser = 1;
                        connectionGroup = oGeneralData.GetProperty("U_GrupoPermisos");
                    }
                    else
                    {
                        companyConnection.Disconnect();
                        companyConnection = null;
                        connectionGroup = null;
                    }
                    
                } catch (Exception ex)
                {
                    companyConnection.Disconnect();
                    companyConnection = null;
                    connectionGroup = null;
                    System.Diagnostics.Debug.WriteLine("EXCEPTION!! : " + ex);
                    _isSAPUser = -1;
                }
                
            }
            else
            {
                System.Diagnostics.Debug.WriteLine("SAP error: " + _connectionError);
                _isSAPUser = -1;
                companyConnection.Disconnect();
                companyConnection = null;
                connectionGroup = null;
            }


            return _isSAPUser;
        }


        /// <summary>
        /// Tries to connect an user to SAP. If the config says that connection is shared, it will use the general
        /// data, else, it will try to do it with the user data. Anyway, it will return the connection data if it
        /// is sucessful
        /// </summary>
        /// <param name="_username">SAP username</param>
        /// <param name="_password">SAP password</param>
        /// <param name="connectionGroup">It returns the connection group of the user (depends on config)</param>
        /// <param name="companyConnection">It returns the connection data for the connection list</param>
        /// <returns></returns>
        public static int ConnectUser (string _username, string _password, out string connectionGroup,
                                        out CompanyConnection companyConnection)
        {
            int userAllowed = 0;
            
            if (ConnectionsManager.connectionMode == ConnectionsManager.ConnectionMode._SharedConnection)
            {
                /*userAllowed = CheckUserOnSAP(_username, _password,
                                            "WIN-VVJ7S13KNNU\\SQLEXPRESS", "SBODEMOES", "manager", "admin",
                                            SAPbobsCOM.BoDataServerTypes.dst_MSSQL2008,
                                            out connectionGroup, out companyConnection);*/
                userAllowed = CheckUserOnSAP(_username, _password,
                                            "ALQUISILLASSAP:30000", "ALQUISILLASSAP\\SQLEXPRESS",
                                            "SBODEMOES", "ecasado", "admin",
                                            SAPbobsCOM.BoDataServerTypes.dst_MSSQL2008,
                                            out connectionGroup, out companyConnection);
                connectionGroup = "main";
            }
            else
            {
                /*userAllowed = CheckUserOnSAP(_username, _password,
                                            "WIN-VVJ7S13KNNU\\SQLEXPRESS", "SBODEMOES", _username, _password,
                                            SAPbobsCOM.BoDataServerTypes.dst_MSSQL2008,
                                            out connectionGroup, out companyConnection);*/
                userAllowed = CheckUserOnSAP(_username, _password,
                                            "ALQUISILLASSAP:30000", "ALQUISILLASSAP\\SQLEXPRESS",
                                            "SBODEMOES", _username, _password,
                                            SAPbobsCOM.BoDataServerTypes.dst_MSSQL2008,
                                            out connectionGroup, out companyConnection);
            }

            return userAllowed;
        }

        private static int MakeConnection(string _licenseServer, string _server, string _companyDB,
                                            string _user, string _password,
                                            SAPbobsCOM.BoDataServerTypes _dbServerType,
                                            string groupName)
        {
            CompanyConnection cConnection =
                new CompanyConnection(_licenseServer, _server, _companyDB, _user, _password, _dbServerType);

            int _error = cConnection.Connect();
            if (_error == 0)
            {
                cConnection.AddToTimeoutEvents();
                ConnectionsManager.AddConnection(groupName, cConnection);
            }

            return _error;
        }

        /*public static int ConnectGroup(string _group)
        {
            int _error;
            switch (_group)
            {
                case "main":
                    _error = MakeConnection("WIN-VVJ7S13KNNU\\SQLEXPRESS", "SBODEMOES", "manager", "admin",
                                            SAPbobsCOM.BoDataServerTypes.dst_MSSQL2008, "main");
                    break;
                default:
                    _error = -1;
                    break;
            }

            return _error;
        }*/

        /// <summary>
        /// Check in SAP the group of the user
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        /*public static string GetGroup(string _username, string _password)
        {
            string _grupo;

            switch (ConnectionsManager.connectionMode)
            {
                case ConnectionsManager.ConnectionMode._SharedConnection:
                    _grupo = "main";
                    break;
                case ConnectionsManager.ConnectionMode._UserConnection:

                    break;
            }


            return _grupo;
        }

        public static string GetGroup(string _token)
        {
            return "main";
        }*/
    }
}