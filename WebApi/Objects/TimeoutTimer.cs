﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Timers;

namespace PruebaRendimiento02_web.Objects
{
    public static class TimeoutTimer
    {
        private static Timer _timer;

        public static void SetTimer(int _ms)
        {
            _timer = new Timer();
            _timer.Interval = _ms;
            _timer.AutoReset = true;
            //_timer.Elapsed += new ElapsedEventHandler((sender, e) => FinDeTiempo(sender, e, this));
            _timer.Start();
        }

        public static Timer get ()
        {
            return _timer;
        }

}
}