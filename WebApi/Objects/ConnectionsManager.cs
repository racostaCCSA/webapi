﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PruebaRendimiento02_web.Objects
{
    public static class ConnectionsManager
    {

        public enum ConnectionMode { _UserConnection, _SharedConnection };
        public static ConnectionMode connectionMode = ConnectionMode._SharedConnection;


        /// <summary>
        /// List of connections, saved as a pair of 'name of group'-'CompanyConection object'
        /// </summary>
        static Dictionary<string, CompanyConnection> companyConnectionList =
            new Dictionary<string, CompanyConnection>();

        /// <summary>
        /// Listof users conected to the application
        /// </summary>
        static List<UserConnection> userConnectionList = new List<UserConnection>();


        
        
        /// <summary>
        /// Adds a connection with the _groupName as a key, in the list. If the connection already exists, it
        /// replaces the old one with the new, so UserConnections of the same group should update it
        /// </summary>
        /// <param name="_groupName"></param>
        /// <param name="_connection"></param>
        public static void AddConnection (string _groupName, CompanyConnection _connection)
        {
            if (companyConnectionList.ContainsKey(_groupName))
            {
                //If there is already a connection for this group, we must delete it.
                //For that, if a conection is disconnected, the users will stop using it,
                //so we only need to stop referencing it on this list
                companyConnectionList[_groupName].Disconnect();
                companyConnectionList.Remove(_groupName);
            }
            companyConnectionList.Add(_groupName, _connection);
        }

        
        public static CompanyConnection GetConnection (string _group)
        {
            return companyConnectionList[_group];
        }

        //private static string 


        private static int CountCompanyConnection (string groupConnection)
        {
            int contConnections = 0;
            
            foreach (UserConnection _elem in userConnectionList)
            {
                if (_elem.connectionGroup == groupConnection)
                    contConnections++;
            }

            return contConnections;
        }


        /// <summary>
        /// It's adds an user to the list of users if it exists on SAP. It's called only on login
        /// screen of the app.
        /// If the user already exists in the list and is a valid one...
        /// </summary>
        /// <param name="_user">Class with user info, where only user and passwor are mandatory.</param>
        /// <returns></returns>
        public static void AddUser (UserConnection _user)
        {
            bool _found = false;
            
            //First, look if user exists on list (has to have the same user name and token
            _found = FindUser(_user);
            //If exists, delete it!
            if (_found)
            {
                DeleteUser(_user.username, _user.token);
            }
            userConnectionList.Add(_user);
            _user.AddToTimeoutEvents();
        }

        private static bool FindUser (UserConnection _user)
        {
            bool _found = false;
            int cont = 0;

            while (!_found && (cont < userConnectionList.Count))
            {
                if ((userConnectionList[cont].username == _user.username) &&
                    (userConnectionList[cont].token == _user.token))
                {
                    _found = true;
                }
                cont++;
            }

            return _found;
        }

        public static void DeleteUser (UserConnection _user)
        {
            //Hay que comprobar si hay más usuarios compartiendo su conexión a SAP, de lo
            //contrario deberíamos cerrar también dicha conexión
            System.Diagnostics.Debug.WriteLine("Esta conexión tiene los siguientes usuarios: " +
                                                CountCompanyConnection(_user.connectionGroup));
            if (_user.connectionGroup != null)
            {
                if (CountCompanyConnection(_user.connectionGroup) == 1)
                {
                    GetConnection(_user.connectionGroup).Disconnect();
                }
            }

            if (userConnectionList.Contains(_user))
            {
                _user.DeleteFromTimeoutEvents();
                userConnectionList.Remove(_user);
            }

            System.Diagnostics.Debug.WriteLine("Número de usuarios actualmente: " +
                userConnectionList.Count().ToString());
        }

        private static void DeleteUser (string _user, string _token)
        {
            if (!String.IsNullOrWhiteSpace(_user) && !String.IsNullOrWhiteSpace(_token))
            {
                bool _found = false;
                int cont = 0;

                while (!_found && (cont < userConnectionList.Count))
                {
                    if ((userConnectionList[cont].username == _user) &&
                        (userConnectionList[cont].token == _token))
                    {
                        userConnectionList[cont].DeleteFromTimeoutEvents();
                        userConnectionList.RemoveAt(cont);
                        _found = true;
                    }
                    cont++;
                }
            }
        }

        public static int TryToConnectUser (string _user, string _password, out UserConnection newUser)
        {
            int _allowed = 0;
            string connectionGroup;
            CompanyConnection companyConnection;
            string newToken;

            newUser = null;
            _allowed = SAPConnections.ConnectUser(_user, _password, out connectionGroup, out companyConnection);
            //Si la identificación del usuario en SAP ha sido positiva, aprovechamos para guardar la conexión
            if (_allowed == 1)
            {

                newToken = HttpServerUtility.UrlTokenEncode(System.Text.Encoding.ASCII.GetBytes(_user + _password));
                newUser = new UserConnection(_user, _password, newToken);
                newUser.connectionGroup = connectionGroup;
                AddUser(newUser);
                companyConnection.AddToTimeoutEvents();
                
                AddConnection(connectionGroup, companyConnection);
                
            }

            return _allowed;
        }

        // FIND IN LISTS ******************************************************************
        /*private static UserConnection FindUserConnectionByName (string _username)
        {
            UserConnection userConection = null;
            int cont = 0;

            while ((cont < userConnectionList.Count()) && (userConection == null))
            {
                if (userConnectionList[cont].username == _username)
                    userConection = userConnectionList[cont];
                cont++;
            }

            return userConection;
        }*/

        public static UserConnection FindUserConnectionByToken(string _token)
        {
            UserConnection userConnection = null;
            int cont = 0;

            while ((cont < userConnectionList.Count()) && (userConnection == null))
            {
                if (userConnectionList[cont].token == _token)
                    userConnection = userConnectionList[cont];
                cont++;
            }

            return userConnection;
        }
        //********************************************************************************

        /*public static SAPbobsCOM.Company GetCompanyByUser (string _username, bool _updateDate)
        {
            SAPbobsCOM.Company oCompany = null;
            UserConnection uConnection = FindUserConnectionByName(_username);

            if (uConnection != null)
            {
                //If there is no conected user with this name, user must put user and password again
                if ((uConnection.oCompanyConnection != null) &&
                    (companyConnectionList.ContainsValue(uConnection.oCompanyConnection)))
                {
                    //If there is no conection, user must put user and password again
                    oCompany = uConnection.oCompanyConnection.GetCompany(_updateDate);
                    //If there is a connection object, but it isn't connected, user must put user and
                    //password again to connect
                    if ((oCompany != null) && !oCompany.Connected)
                        oCompany = null;
                }

            }

            return oCompany;
        }*/

        public static SAPbobsCOM.Company GetCompanyByToken(string _token, bool _updateDate)
        {
            SAPbobsCOM.Company oCompany = null;
            UserConnection uConnection = FindUserConnectionByToken(_token);
            CompanyConnection companyConnection;
            string _group;
            int _errorConnection;

            if (uConnection != null)
            {
                //If there is no conected user with this name, user must put user and password again
                //Looking if the conection of this user exists
                if (!String.IsNullOrWhiteSpace(uConnection.connectionGroup) &&
                    (companyConnectionList.ContainsKey(uConnection.connectionGroup)))
                {
                    oCompany = GetConnection(uConnection.connectionGroup).GetCompany(_updateDate);
                }

                //if uConection has oCompanyConection as null, or it's oCompany as null, or it's
                //conection has expired (is not on the companyConectionList), we'll try to conect
                //again
                if ((oCompany == null) || !oCompany.Connected)
                {
                    System.Diagnostics.Debug.WriteLine("Conection not found! Trying to connect!");
                    _group = uConnection.connectionGroup; //Finding group to connect

                    //There is 3 reasons to reconnect:
                    //- There is no group
                    //- the group not exists on companyConnectionList
                    //- The connection for this group is disconnected (and disposed by the system)
                    _errorConnection = 0;
                    if ((String.IsNullOrWhiteSpace(_group)) || (!companyConnectionList.ContainsKey(_group)) ||
                        (!companyConnectionList[_group].GetCompany(false).Connected))
                    {
                        if (1 == SAPConnections.ConnectUser(uConnection.username, uConnection.password, out _group,
                                                            out companyConnection))
                        {
                            companyConnection.AddToTimeoutEvents();
                            ConnectionsManager.AddConnection(_group, companyConnection);
                            _errorConnection = 0;
                        }
                        else
                            _errorConnection = 1;
                    }
                    


                    if (_errorConnection == 0)
                    {
                        //Adding conection to the user
                        uConnection.connectionGroup = _group;
                        //Returning Company
                        oCompany = companyConnectionList[_group].GetCompany(_updateDate);
                    }
                    else
                    {
                        oCompany = null;
                    }
                }
            }
            else
            {
                System.Diagnostics.Debug.WriteLine("User token '" + _token + "' not found!");
            }

            return oCompany;
        }

    }
}