﻿using PruebaRendimiento02_web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;

namespace PruebaRendimiento02_web.Objects
{
    //This works OK, but i need to try another method that don't read directly from the URI
    public class MyAuthorizeAttribute : AuthorizeAttribute
    {
        protected override bool IsAuthorized (HttpActionContext actionContext)
        {
            //At this point the data are not binded, so the only way to retreive the token is reading it from the URI
            bool _authorized = false;

            System.Collections.Specialized.NameValueCollection queryData = HttpUtility.ParseQueryString(actionContext.Request.RequestUri.Query);
            string _token = queryData["oauth_token"];
            if (ConnectionsManager.FindUserConnectionByToken(_token) != null)
                _authorized = true;

            return _authorized;
        }
    }
}