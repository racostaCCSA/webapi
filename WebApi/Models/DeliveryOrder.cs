﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApi
{
    public class DeliveryOrder
    {

        public int OrderID { get; set; }
        public int LnID { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public int UnitsContainer { get; set; }
        public int UnitsToPrepare { get; set; }
        public int UnitsPrepared { get; set; }
        public int UnitsPending { get; set; }
        public int UnitsToReduce { get; set; }
        public List<SerialNumber> LSerialNumber { get; set; }
        public string TokenID { get; set; }
        public int _WorkOrderID;

        public DeliveryOrder()
        {
            Code = "";
            Name = "";
            UnitsContainer = 0;
            UnitsToPrepare = 0;
            UnitsPrepared = 0;
            UnitsPending = 0;
            UnitsToReduce = 0;
        }

        public DeliveryOrder(int iWOID, int iOrderID, int iLnID)
        {
            Code = "Code" + iOrderID.ToString();
            Name = "Name " + Code;
            UnitsContainer = iLnID;
            UnitsToPrepare = 15;
            UnitsPrepared = 10;
            UnitsPending = UnitsToPrepare - UnitsPrepared;

            _WorkOrderID = iWOID;
        }


    }
}