﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApi
{
    public class PickUpOrder
    {
        public int OrderID { get; set; }
        public int LnID { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public int UnitsContainer { get; set; }
        public int UnitsDraft { get; set; }
        public int UnitsToPickUp { get; set; }
        public int UnitsPickedUp { get; set; }
        public int UnitsPickedUpOk { get; set; }
        public int UnitsPickedUpBad { get; set; }
        public int UnitsPickedUpRecover { get; set; }
        public int UnitsPending { get; set; }
        public List<SerialNumber> LSerialNumber { get; set; }
        public string TokenID { get; set; }

        public int _WorkOrderID;

        public PickUpOrder()
        {
            Code = "";
            Name = "";
            UnitsContainer = 0;
            UnitsDraft = 0;
            UnitsToPickUp = 0;
            UnitsPickedUp = 0;
            UnitsPickedUpOk = 0;
            UnitsPickedUpBad = 0;
            UnitsPickedUpRecover = 0;
            UnitsPending = 0;
        }

        public PickUpOrder(int iWOID, int iOrderID, int iLnID)
        {
            Code = "Code" + iOrderID.ToString();
            Name = "Name " + Code;
            UnitsContainer = iLnID;
            UnitsDraft = 25;
            UnitsToPickUp = 25;
            UnitsToPickUp = 15;
            UnitsPickedUpOk = 10;
            UnitsPickedUpBad = UnitsToPickUp - UnitsPickedUpOk;
            UnitsPending = UnitsToPickUp - UnitsPickedUpOk - UnitsPickedUpBad;
            UnitsPickedUpRecover = 0;
            _WorkOrderID = iWOID;

        }


    }
}