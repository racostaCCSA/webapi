﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApi
{
    public class TaskOrder
    {
        public int OrderID { get; set; }
        public int LnID { get; set; }
        public bool Finished { get; set; }
        public int Type { get; set; }
        public string JobType { get; set; }
        public string CodeEmp { get; set; }
        public string NameEmp { get; set; }
        public string Comment { get; set; }
        public DateTime DateIni { get; set; }
        public DateTime DateEnd { get; set; }
        // public int HourIni;
        //  public int HourEnd;

        public int _WorkOrderID;
        public TaskOrder()
        {
            OrderID = 0;
            LnID = 0;
            Finished = false;
            Type = 0;
            JobType = "";
            DateIni = DateTime.Now;
            DateEnd = DateTime.Now;
            CodeEmp = "";

        }

        public TaskOrder(int iWOID, int iOrderID, int iLnID)
        {
            OrderID = iOrderID;
            LnID = iLnID;
            Finished = false;
            Type = 10;
            JobType = "Carga";
            DateIni = DateTime.Now;
            DateEnd = DateTime.Now;
            _WorkOrderID = iWOID;
        }


    }
}