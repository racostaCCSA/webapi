﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApi
{
    public class WorkOrder
    {

        public WorkOrder()
        {
            OrderID = 0;
            OrderType = 0;
            Date = DateTime.Now;
            HourIni = "0";
            HourEnd = "0";
            Status = "";
        //    Description = "";
        }

        public WorkOrder(int iId, int iType, DateTime dDate, string iHIni, string iHEnd, string sStatus, string sDes)
        {
            OrderID = iId;
            OrderType = iType;
            Date = dDate;
            HourIni = iHIni;
            HourEnd = iHEnd;
            Status = sStatus;
      //      Description = sDes;
        }

        public int OrderID { get; set; }
        public int OrderType { get; set; }
        public DateTime Date { get; set; }
        public string HourIni { get; set; }
        public string HourEnd { get; set; }
        public string Status { get; set; }
        public string CardCode { get; set; }
        public string CardName { get; set; }
        public string PrjCode { get; set; }
        public string PlaceCode { get; set; }
        public string PlaceName { get; set; }

       // public string Description { get; set; }



    }
}