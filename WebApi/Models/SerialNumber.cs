﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApi
{
    public class SerialNumber
    {
        public SerialNumber()
        {
            Code = "";
            Units = 0;
        }


        public SerialNumber(string sCode, int iUnits)
        {
            Code = sCode;
            Units = iUnits;
        }
        public string Code { get; set; }
        public int Units { get; set; }
    }

}
