﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApi.Defines;
using WebApi.Services;



//DocEntry    int Unchecked
//LineId  int Unchecked
//VisOrder    int Checked
//Object nvarchar(20)	Checked
//LogInst int Checked
//U_CCS_CodA nvarchar(20)	Checked
//U_CCS_NomA  nvarchar(100)   Checked
//U_CCS_UdAP  int Checked
//U_CCS_UdPr  int Checked
//U_CCS_UdPt  int Checked
//U_CCS_Peso numeric(19, 6)	Checked
//U_CCS_NSer  char(1) Checked
//U_CCS_UdRd  int Checked                   - Unidades a Reducir
//U_CCS_UdCn  int Checked                   - Unidadades contenedor
//U_CCS_Cnt nvarchar(254)	Checked
//U_CCS_Vol   numeric(19, 6)  Checked

namespace WebApi.Controllers
{

    public class DeliveryOrderController : ApiController
    {
        private SAPbobsCOM.Company oCompany;


        // GET: api/DeliveryOrder
        public IEnumerable<DeliveryOrder> Get(int id)
        {
            oCompany = ConnectSAPConfig.EnsureCompany();
            var ListDO = new List<DeliveryOrder>();

            if (oCompany != null)
            {

                SAPbobsCOM.Recordset oRS = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                oRS.DoQuery(String.Format(Querys.DeliverysOrders, id));
                while (!oRS.EoF)
                {
                    DeliveryOrder oDO = new DeliveryOrder();

                    oDO.OrderID = (int)oRS.Fields.Item("DocEntry").Value;
                    oDO.LnID = (int)oRS.Fields.Item("LineId").Value;
                    oDO.Code = (string)oRS.Fields.Item("U_CCS_CodA").Value;
                    oDO.Name = (string)oRS.Fields.Item("U_CCS_NomA").Value;

                    oDO.UnitsContainer = Convert.ToInt16(oRS.Fields.Item("U_CCS_UdCn").Value);
                    oDO.UnitsToPrepare = Convert.ToInt16(oRS.Fields.Item("U_CCS_UdAP").Value);
                    oDO.UnitsPrepared = Convert.ToInt16(oRS.Fields.Item("U_CCS_UdPr").Value);
                    oDO.UnitsPending = Convert.ToInt16(oRS.Fields.Item("U_CCS_UdPt").Value);
                    oDO.UnitsToReduce = Convert.ToInt16(oRS.Fields.Item("U_CCS_UdRd").Value);


                    ListDO.Add(oDO);

                    oRS.MoveNext();

                }
            }
            return ListDO;
        }

        // POST: api/DeliveryOrder
        public HttpResponseMessage Post([FromBody]DeliveryOrder oDO)
        {
            HttpResponseMessage response = null;
            var ListValues = new List<FieldSAP>();

            oCompany = ConnectSAPConfig.EnsureCompany();

            if (oCompany == null)
            {
                response = new HttpResponseMessage(HttpStatusCode.Forbidden);
            }
            else
            {
                // Rellenamos los campos de SAP con sul valores
                var value = new FieldSAP("U_CCS_UdAP", oDO.UnitsToPrepare);
                ListValues.Add(value);
                value = new FieldSAP("U_CCS_UdPr", oDO.UnitsPrepared);
                ListValues.Add(value);
                value = new FieldSAP("U_CCS_UdPt", oDO.UnitsPending);
                ListValues.Add(value);
                value = new FieldSAP("U_CCS_UdRd", oDO.UnitsToReduce);
                ListValues.Add(value);





                if (ServicesSAP.SetPropertyChild(oCompany, "CCS_PREPARACION", "CCS_PREPARACION1", SAPbobsCOM.BoUDOObjType.boud_Document, oDO.OrderID.ToString(), oDO.LnID, ListValues))
                {
                   response = new HttpResponseMessage(HttpStatusCode.OK);
                }
                if (response == null)
                    response = new HttpResponseMessage(HttpStatusCode.ExpectationFailed);

            }

            return response;            

        }

    }
}
