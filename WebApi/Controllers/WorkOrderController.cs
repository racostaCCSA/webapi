﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApi.Defines;
using WebApi.Services;


namespace WebApi.Controllers
{
    public class WorkOrderController : ApiController
    {

        private SAPbobsCOM.Company oCompany;

        // GET: api/WorkOrder
        public IEnumerable<WorkOrder> Get()
        {
            oCompany = ConnectSAPConfig.EnsureCompany();
            var ListWO = new List<WorkOrder>();

            if (oCompany != null)
            {

                SAPbobsCOM.Recordset oRS = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                oRS.DoQuery(Querys.OrdersWorks);
                

                
                while (!oRS.EoF)
                {
                    WorkOrder oWO = new WorkOrder();

                    oWO.OrderID = (int)oRS.Fields.Item("DocNum").Value;
                    oWO.OrderType = (int)oRS.Fields.Item("Tipo").Value;
                    oWO.Date = (DateTime)oRS.Fields.Item("U_CCS_FecIniT").Value;
                    oWO.HourIni = DateSAP.ViewHourFromSAP(oRS.Fields.Item("U_CCS_HorIniT").Value);
                    oWO.HourEnd = DateSAP.ViewHourFromSAP(oRS.Fields.Item("U_CCS_HorFinT").Value);
                    string s = oRS.Fields.Item("U_CCS_Estado").Value;
                    if (oWO.OrderType == (int)OrderType.Delivery)
                    {
                        switch (s)
                        { 
                            case "1":
                                
                                oWO.Status = StatusDelivery.Pendiente.ToString(); //"Pendiente";
                            break;
                            case "2":
                                oWO.Status = StatusDelivery.Abierta.ToString();
                                break;
                            case "3":
                                oWO.Status = StatusDelivery.Cerrada.ToString();
                                break;
                        }
                    }
                    else
                    {
                        switch (s)
                        {
                            case "1":
                                oWO.Status = EnumF.GetDescriptionAttribute(StatusPickUp.Pendiente);
                                break;
                            case "2":
                                oWO.Status = EnumF.GetDescriptionAttribute(StatusPickUp.Transito);
                                break;
                            case "3":
                                oWO.Status = EnumF.GetDescriptionAttribute(StatusPickUp.Cerrada);
                                break;
                            case "4":
                                oWO.Status = EnumF.GetDescriptionAttribute(StatusPickUp.Conteo);
                                break;
                        }

                    }

                    oWO.CardCode = (string)oRS.Fields.Item("U_CCS_CodCli").Value;
                    oWO.CardName = (string)oRS.Fields.Item("U_CCS_NomCli").Value;
                    oWO.PrjCode = (string)oRS.Fields.Item("U_CCS_Proyecto").Value;
                    oWO.PlaceCode = "";
                    oWO.PlaceName = (string)oRS.Fields.Item("U_CCS_FncDes").Value;


                    ListWO.Add(oWO);

                    oRS.MoveNext();

                }
            }
            return ListWO;


        }



        //public IEnumerable<string> Get()
        //{
        //    return new string[] { "value1", "value2" };
        //}

        // GET: api/WorkOrder/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/WorkOrder
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/WorkOrder/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/WorkOrder/5
        public void Delete(int id)
        {
        }
    }
}
