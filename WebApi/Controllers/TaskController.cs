﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApi.Defines;
using WebApi.Services;


namespace WebApi.Controllers
{
    public class TaskController : ApiController
    {
        private SAPbobsCOM.Company oCompany;

        // GET: api/PickUpOrder
        // public IEnumerable<TaskOrder> Get([FromBody]TaskOrder to)
        public IEnumerable<TaskOrder> Get(int id)
        {
           
            var ListTO = new List<TaskOrder>();
            TaskOrder oTO = new TaskOrder();
            ListTO.Add(oTO);


            return ListTO;
        }
            public IEnumerable<TaskOrder> Get(int id, int id2)
        {
            DateTime date;
            

            oCompany = ConnectSAPConfig.EnsureCompany();
            
            var ListTO = new List<TaskOrder>();

            if (oCompany != null)
            {

                SAPbobsCOM.Recordset oRS = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                if (id == (int)OrderType.Delivery)
                    oRS.DoQuery(String.Format(Querys.DeliverysTasks, id2));
                else
                    oRS.DoQuery(String.Format(Querys.PickUpsTasks, id2));

                while (!oRS.EoF)
                {
                    TaskOrder oTO = new TaskOrder();

                    oTO.Type = id;
                    oTO.OrderID = (int)oRS.Fields.Item("DocEntry").Value;
                    oTO.LnID = (int)oRS.Fields.Item("LineId").Value;
                    oTO.JobType = (string)oRS.Fields.Item("U_CCS_Job").Value;
                    
                        
                    date = oRS.Fields.Item("U_CCS_FecIni").Value;
                    var hour = oRS.Fields.Item("U_CCS_HorIni").Value;

                    oTO.DateIni = DateSAP.GetTimeFromSAP(date, hour);


                    date = oRS.Fields.Item("U_CCS_FecFin").Value;
                    hour = oRS.Fields.Item("U_CCS_HorFin").Value;

                    oTO.DateEnd = DateSAP.GetTimeFromSAP(date, hour);


                    var done = oRS.Fields.Item("U_CCS_Done").Value;
                    if (done == "Y")
                        oTO.Finished = true;
                    else
                        oTO.Finished = false;


                    oTO.Comment = oRS.Fields.Item("U_CCS_Comnt").Value;
                    oTO.CodeEmp = oRS.Fields.Item("U_CCS_CodE").Value;
                    oTO.NameEmp  = oRS.Fields.Item("U_CCS_NomE").Value;

                    ListTO.Add(oTO);

                    oRS.MoveNext();

                }
            }
            return ListTO;
        }

        // POST: api/PickUpOrder
        public HttpResponseMessage Post([FromBody]TaskOrder oTO)
        {
            HttpResponseMessage response = null;
            string temval;
            int ival;
            var ListValues = new List<FieldSAP>();

            oCompany = ConnectSAPConfig.EnsureCompany();

            if (oCompany == null)
            {
                response = new HttpResponseMessage(HttpStatusCode.Forbidden);
            }
            else
            {

                // Rellenamos los campos de SAP con sul valores
                if (oTO.Finished)
                    temval = "Y";
                else
                    temval = "N";
                var value = new FieldSAP("U_CCS_Done", temval);
                ListValues.Add(value);

             //   temval = DateSAP.DateToSAP(oTO.DateIni);
                value = new FieldSAP("U_CCS_FecIni", oTO.DateIni);
                ListValues.Add(value);

            //    ival = DateSAP.HourToSAP(oTO.DateIni);
                value = new FieldSAP("U_CCS_HorIni", oTO.DateIni);
                ListValues.Add(value);
              
             //   temval = DateSAP.DateToSAP(oTO.DateEnd);
                value = new FieldSAP("U_CCS_FecFin", oTO.DateEnd);
                ListValues.Add(value);

           //     ival = DateSAP.HourToSAP(oTO.DateEnd);
                value = new FieldSAP("U_CCS_HorFin", oTO.DateEnd);
                ListValues.Add(value);



                if (oTO.Type == (int)OrderType.Delivery)
                {
                        if (ServicesSAP.SetPropertyChild(oCompany, "CCS_PREPARACION", "CCS_PREPARACION2", SAPbobsCOM.BoUDOObjType.boud_Document, oTO.OrderID.ToString(), oTO.LnID, ListValues))
                            response = new HttpResponseMessage(HttpStatusCode.OK);
                }
                else
                {
                        if (ServicesSAP.SetPropertyChild(oCompany, "CCS_RECOGIDA", "CCS_RECOGIDA2", SAPbobsCOM.BoUDOObjType.boud_Document, oTO.OrderID.ToString(), oTO.LnID, ListValues))
                            response = new HttpResponseMessage(HttpStatusCode.OK);
                }
                if (response == null)
                    response = new HttpResponseMessage(HttpStatusCode.ExpectationFailed);

            }

            return response;

        }


    }
}
