﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApi.Defines;
using WebApi.Services;


//DocEntry int Unchecked
//LineId int Unchecked
//VisOrder int Checked
//Object nvarchar(20)    Checked
//LogInst int Checked
//U_CCS_CodA nvarchar(20)    Checked
//U_CCS_NomA  nvarchar(100)   Checked
//U_CCS_BrUR  int Checked
//U_CCS_UdAP int Checked
//U_CCS_UdPr int Checked
//U_CCS_UdPt int Checked
//U_CCS_Peso numeric(19, 6)  Checked
//U_CCS_NSer  char (1)	Checked
//U_CCS_UdME  int Checked
//U_CCS_UdMERc int Checked
//U_CCS_UdBE int Checked
//U_CCS_UdCn int Checked
//U_CCS_Cnt nvarchar(254)   Checked
//U_CCS_Vol   numeric(19, 6)  Checked
//      Unchecked

namespace WebApi.Controllers
{
    public class PickUpOrderController : ApiController
    {
        private SAPbobsCOM.Company oCompany;


        // GET: api/PickUpOrder
        public IEnumerable<PickUpOrder> Get(int id)
        {
            oCompany = ConnectSAPConfig.EnsureCompany();
            var ListPO = new List<PickUpOrder>();

            if (oCompany != null)
            {

                SAPbobsCOM.Recordset oRS = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                oRS.DoQuery(String.Format(Querys.PickUpsOrders,id));
                while (!oRS.EoF)
                {
                    PickUpOrder oPO = new PickUpOrder();

                    oPO.OrderID = (int)oRS.Fields.Item("DocEntry").Value;
                    oPO.LnID = (int)oRS.Fields.Item("LineId").Value;
                    oPO.Code = (string)oRS.Fields.Item("U_CCS_CodA").Value;
                    oPO.Name = (string)oRS.Fields.Item("U_CCS_NomA").Value;

                    oPO.UnitsContainer = Convert.ToInt16(oRS.Fields.Item("U_CCS_UdCn").Value); //Unidad.Contenedores
                    oPO.UnitsToPickUp = Convert.ToInt16(oRS.Fields.Item("U_CCS_UdAP").Value); // Unidades a Recoger
                    oPO.UnitsPickedUp = Convert.ToInt16(oRS.Fields.Item("U_CCS_UdPr").Value);  // Unidades recogidas
                    oPO.UnitsPickedUpOk = Convert.ToInt16(oRS.Fields.Item("U_CCS_UdBE").Value); // Ud. Buen Estado
                    oPO.UnitsPickedUpBad = Convert.ToInt16(oRS.Fields.Item("U_CCS_UdME").Value); //Ud. Mal Estado
                    oPO.UnitsPickedUpRecover = Convert.ToInt16(oRS.Fields.Item("U_CCS_UdMERc").Value); // Ud. Mal estado recuperable
                    oPO.UnitsPending = Convert.ToInt16(oRS.Fields.Item("U_CCS_UdPt").Value); // Ud. Pendientes

                   
                    oPO.UnitsDraft = Convert.ToInt16(oRS.Fields.Item("U_CCS_BrUR").Value); // Br. Ud A Recoger
                    
                    
                    
                    
                    

                    ListPO.Add(oPO);

                    oRS.MoveNext();

                }
            }
            return ListPO;
        }

        // POST: api/PickUpOrder
        public HttpResponseMessage Post([FromBody]PickUpOrder oPO)
        {
            HttpResponseMessage response = null;
            var ListValues = new List<FieldSAP>();

            oCompany = ConnectSAPConfig.EnsureCompany();

            if (oCompany == null)
            {
                response = new HttpResponseMessage(HttpStatusCode.Forbidden);
            }
            else
            {

                // Rellenamos los campos de SAP con sul valores
                var value = new FieldSAP("U_CCS_UdAP", oPO.UnitsToPickUp);
                ListValues.Add(value);
                value = new FieldSAP("U_CCS_UdPr", oPO.UnitsPickedUp);
                ListValues.Add(value);
                value = new FieldSAP("U_CCS_UdME", oPO.UnitsPickedUpBad);
                ListValues.Add(value);
                value = new FieldSAP("U_CCS_UdMERc", oPO.UnitsPickedUpRecover);
                ListValues.Add(value);
                value = new FieldSAP("U_CCS_UdBE", oPO.UnitsPickedUpOk);
                ListValues.Add(value);
                value = new FieldSAP("U_CCS_UdPt", oPO.UnitsPending);
                ListValues.Add(value);

                if (ServicesSAP.SetPropertyChild(oCompany, "CCS_RECOGIDA", "CCS_RECOGIDA1", SAPbobsCOM.BoUDOObjType.boud_Document, oPO.OrderID.ToString(), oPO.LnID, ListValues))
                {
                        response = new HttpResponseMessage(HttpStatusCode.OK);
                }
                if (response == null)
                    response = new HttpResponseMessage(HttpStatusCode.ExpectationFailed);

            }

            return response;

        }


    }
}
