﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApi.Services
{
    public class DateSAP
    {


        public static string HourFromSAP(int hour)
        {
            return string.Format("{0:0000}", hour);
        }
        public static string ViewHourFromSAP(int hour)
        {
            return string.Format("{0:00:00}", hour);
        }
        //'<DebuggerStepThrough()>
        //''' <summary>
        //''' Convierte una cadena formato fecha-hora de SAP (HHNN) a tipo Date.
        //''' </summary>
        //''' <param name="DateString"></param>
        //''' <returns></returns>
        //''' <exception cref="Exception">Todo error es devuelto al método de llamada. No se comprueba que el parámetro tenga el formato adecuado.</exception>
        public static DateTime GetTimeFromSAP(DateTime date, string hour)
        {
            DateTime result;
            try
            {
                result = new DateTime(date.Year, date.Month, date.Day, Convert.ToInt16(hour.Substring(0, 2)), Convert.ToInt16(hour.Substring(2, 2)), 0);
            }
            catch
            {
                throw;
            }
                return result;
        }

        public static DateTime GetTimeFromSAP(DateTime date, int hour)
        {
            DateTime result;
            string hourstring;
            try
            {
                hourstring = HourFromSAP(hour);
               

                result = new DateTime(date.Year, date.Month, date.Day, Convert.ToInt16(hourstring.Substring(0, 2)), Convert.ToInt16(hourstring.Substring(2, 2)), 0);
            }
            catch
            {
                throw;
            }
            return result;
        }

        //''' <summary>
        //   ''' Convierte una cadena formato fecha de SAP (yyyyMMdd) a tipo Date.
        //   ''' </summary>
        //   ''' <param name="DateString"></param>
        //   ''' <returns></returns>
        //   ''' <exception cref="Exception">Todo error es devuelto al método de llamada. No se comprueba que el parámetro tenga el formato adecuado.</exception>
        //   <DebuggerStepThrough()>
        public DateTime GetDateFromSAP(string  DateString)
        {
            DateTime mresult;
            try
            {
                mresult = new DateTime(Convert.ToInt16(DateString.Substring(0, 4)), Convert.ToInt16(DateString.Substring(4, 2)), Convert.ToInt16(DateString.Substring(6, 2)));

            }
            catch
            {
                throw;
            }
            return mresult;
        }
        public static DateTime GetDateTimeFromSAP(string DateString, Int16 Hour)
        {
            DateTime mresult;
            string HourString;
          


            try
            {

                HourString = HourFromSAP(Hour);
               
                mresult = new DateTime(Convert.ToInt16(DateString.Substring(0, 4)), Convert.ToInt16(DateString.Substring(4, 2)), Convert.ToInt16(DateString.Substring(6, 2)), Convert.ToInt16(HourString.Substring(0, 2)), Convert.ToInt16(HourString.Substring(2, 2)), 0);

            }
            catch
            {
                throw;
            }
            return mresult;
        }


    //    ''' <summary>
    //''' Devuelve un string de fecha con formato SAP (yyyyMMdd)
    //''' </summary>
    //''' <param name="Fecha"></param>
    //''' <returns></returns>
    //''' <remarks></remarks>
    public static string DateToSAP (DateTime date)
        {
            try
            {
                return date.ToString("yyyyMMdd");
            }
            catch
            {
                throw;
            }
        }




 //''' <summary>
 //   ''' Convierte una cadena hora en formato 'HH:mm' a su entero equivalente en formato SAP
 //   ''' </summary>
 //   ''' <param name="HourString"></param>
 //   ''' <returns></returns>
 //   ''' <remarks></remarks>
    public static Int16 HourToSAP (DateTime date)
        {
            string hour;
            Int16 result;
            try
            {
                hour = date.ToString("HH:mm");
                var r = Convert.ToInt16(hour.Substring(0, 2)) * 100 + Convert.ToInt16(hour.Substring(3, 2));
                result = (Int16)r;

            }
            catch
            {
                throw;
            }
            return result;

        }

    }
}