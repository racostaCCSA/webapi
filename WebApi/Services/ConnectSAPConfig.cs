﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SAPbobsCOM;
using System.Configuration;
using System.Security.Cryptography;
using System.Text;

namespace WebApi
{
    

    public class ConnectSAPConfig
    {
        public static Company _company = null;
        public static int statusCOM = -1;
        private static int errorCOM = 0;
        private static string msgCOM = "";

        public static string Encrypt(string toEncrypt, bool useHashing)
        {
            byte[] keyArray;
            byte[] toEncryptArray = UTF8Encoding.UTF8.GetBytes(toEncrypt);

            //System.Configuration.AppSettingsReader settingsReader =
                                                new AppSettingsReader();
            // Get the key from config file

            //string key = (string)settingsReader.GetValue("SecurityKey",typeof(String));

            string key = "12345678";
            //System.Windows.Forms.MessageBox.Show(key);
            //If hashing use get hashcode regards to your key
            if (useHashing)
            {
                MD5CryptoServiceProvider hashmd5 = new MD5CryptoServiceProvider();
                keyArray = hashmd5.ComputeHash(UTF8Encoding.UTF8.GetBytes(key));
                //Always release the resources and flush data
                // of the Cryptographic service provide. Best Practice

                hashmd5.Clear();
            }
            else
                keyArray = UTF8Encoding.UTF8.GetBytes(key);

            TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();
            //set the secret key for the tripleDES algorithm
            tdes.Key = keyArray;
            //mode of operation. there are other 4 modes.
            //We choose ECB(Electronic code Book)
            tdes.Mode = CipherMode.ECB;
            //padding mode(if any extra byte added)

            tdes.Padding = PaddingMode.PKCS7;

            ICryptoTransform cTransform = tdes.CreateEncryptor();
            //transform the specified region of bytes array to resultArray
            byte[] resultArray =
              cTransform.TransformFinalBlock(toEncryptArray, 0,
              toEncryptArray.Length);
            //Release resources held by TripleDes Encryptor
            tdes.Clear();
            //Return the encrypted data into unreadable string format
            return Convert.ToBase64String(resultArray, 0, resultArray.Length);
        }
        public static string Decrypt(string cipherString, bool useHashing)
        {
            byte[] keyArray;
            //get the byte code of the string

            byte[] toEncryptArray = Convert.FromBase64String(cipherString);

            //System.Configuration.AppSettingsReader settingsReader = new AppSettingsReader();
            ////Get your key from config file to open the lock!
            //string key = (string)settingsReader.GetValue("SecurityKey",
            //                                             typeof(String));
            string key = "12345678";

            if (useHashing)
            {
                //if hashing was used get the hash code with regards to your key
                MD5CryptoServiceProvider hashmd5 = new MD5CryptoServiceProvider();
                keyArray = hashmd5.ComputeHash(UTF8Encoding.UTF8.GetBytes(key));
                //release any resource held by the MD5CryptoServiceProvider

                hashmd5.Clear();
            }
            else
            {
                //if hashing was not implemented get the byte code of the key
                keyArray = UTF8Encoding.UTF8.GetBytes(key);
            }

            TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();
            //set the secret key for the tripleDES algorithm
            tdes.Key = keyArray;
            //mode of operation. there are other 4 modes. 
            //We choose ECB(Electronic code Book)

            tdes.Mode = CipherMode.ECB;
            //padding mode(if any extra byte added)
            tdes.Padding = PaddingMode.PKCS7;

            ICryptoTransform cTransform = tdes.CreateDecryptor();
            byte[] resultArray = cTransform.TransformFinalBlock(
                                 toEncryptArray, 0, toEncryptArray.Length);
            //Release resources held by TripleDes Encryptor                
            tdes.Clear();
            //return the Clear decrypted TEXT
            return UTF8Encoding.UTF8.GetString(resultArray);
        }

        public static Company EnsureCompany()
        {
            if (_company == null)
            {
                //// Initialize the Company Object.
                //// Create a new company object
                _company = new Company();
                statusCOM = -1;
            }
            if (statusCOM!=0)
            {

                //// Set the mandatory properties for the connection to the database.
                //// here I bring only 2 of the 5 mandatory fields.
                //// To use a remote Db Server enter his name instead of the string "(local)"
                //// This string is used to work on a DB installed on your local machine
                //// the other mandatory fields are CompanyDB, UserName and Password
                //// I am setting those fields in the ChooseCompany Form

                //var cod = Encrypt("admin", true);
                //cod = Encrypt("SAPB1Admin", true);
                //cod = Encrypt("manager", true);


                _company.LicenseServer = ConfigurationManager.AppSettings.Get("LicenseServer"); // "ALQUISILLASSAP:30000"; //"192.168.1.155:30000"; // 
                var pas = ConfigurationManager.AppSettings.Get("DBPassword");
                _company.DbPassword = Decrypt(pas, true); // "SAPB1Admin";
                _company.DbUserName = ConfigurationManager.AppSettings.Get("DBBUserName");
                _company.Server = ConfigurationManager.AppSettings.Get("Server"); //"ALQUISILLASSAP";//"ALQUISILLASSAP\\SQLEXPRESS"; //"WIN-VVJ7S13KNNU\SQLEXPRESS";   
                _company.CompanyDB = ConfigurationManager.AppSettings.Get("DB"); //"PRUEBASALQUISILLAS";//"DEMO_ALQUISILLAS";
                _company.UserName = ConfigurationManager.AppSettings.Get("UserName"); //"ecasado";
                pas = ConfigurationManager.AppSettings.Get("Password"); //"admin";
                _company.Password = Decrypt(pas, true);

                _company.DbServerType = BoDataServerTypes.dst_MSSQL2008;


                _company.language = BoSuppLangs.ln_Spanish;

                //// Use Windows authentication for database server.
                //// True for NT server authentication,
                //// False for database server authentication.
                //oCompany.UseTrusted = true;
                statusCOM = _company.Connect();
                if (statusCOM != 0)
                {
                    _company.GetLastError(out errorCOM, out msgCOM);
                    return null;
                }

                //int error;
                //string msg;
                //_company.GetLastError(out error, out msg);
            }
            return _company;
        }
         
        public void DisconnectSAP()
        {
            if (_company!=null)
            {
                _company.Disconnect();
                _company = null;
            }
        }


    }
}