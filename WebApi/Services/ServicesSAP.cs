﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SAPbobsCOM;
namespace WebApi.Services
{


    public class UdoSAP
    {

    }
    public class FieldSAP
    {
        public string FieldName { get; set; }
        public object Value { get; set; }
        public FieldSAP (string fieldName, object value)
        {
            FieldName = fieldName;
            Value = value;
        }

    }

    public class ServicesSAP
    {

        //''' <summary>
        //    ''' Dado un objeto de lineas de detalle de un UDO devuelve el primer registro (generalData) coincidente con LineId
        //    ''' </summary>
        //    ''' <param name="oLineas"></param>
        //    ''' <param name="LineId"></param>
        //    ''' <returns></returns>
        //    ''' <remarks></remarks>
        private static SAPbobsCOM.GeneralData GetLineaUDO(SAPbobsCOM.GeneralDataCollection oLineas, int lineId)
        {


            SAPbobsCOM.GeneralData GenData = null;
                        bool directo = false;
            try
            {
                try
                {
                    if (Convert.ToInt16(oLineas.Item(lineId - 1).GetProperty("LineId")) == lineId)
                        directo = true;
                }
                catch { }

                if (directo == true)
                {
                    GenData = oLineas.Item(lineId - 1);
                }
                else
                {

                    for (int i = 0; i <= oLineas.Count - 1; i++)
                    {
                        if (Convert.ToInt16(oLineas.Item(i).GetProperty("LineId")) == lineId)
                        {
                            GenData = oLineas.Item(i);
                            break;
                        }
                    }
                }
                return GenData;
            }
            catch
            {
                return null;
            }
            finally
            { 
                GenData = null;
                GC.Collect();
            }

        }
    

    
        //''' <summary>
        //''' Modifica un valor de una tabla de detalle buscando por clave (código tabla maestra y lineId)
        //''' </summary>
        //''' <param name="UDOName"></param>
        //''' <param name="UDOChild"></param>
        //''' <param name="UDOType"></param>
        //''' <param name="MasterCode"></param>
        //''' <param name="LineId"></param>
        //''' <param name="NombreCampo"></param>
        //''' <param name="Valor"></param>
        //''' <returns>True si la operación se realizó con éxito</returns>
        //''' <remarks></remarks>
        public static bool SetPropertyChild(Company oCompany, string uDOName, string uDOChild, BoUDOObjType uDOType, string masterCode, int lineId, List<FieldSAP> listvalues)
        {
            SAPbobsCOM.CompanyService oCompanyService;
            SAPbobsCOM.GeneralDataParams oGeneralParams;
            SAPbobsCOM.GeneralService oService;
            SAPbobsCOM.GeneralData oCabecera;
            SAPbobsCOM.GeneralData olinea;
            int b = 0;

            try
            {
                oCompanyService = oCompany.GetCompanyService();
                oService = oCompanyService.GetGeneralService(uDOName);
                oCabecera = (GeneralData)oService.GetDataInterface(GeneralServiceDataInterfaces.gsGeneralData);
                // Lineas
                oGeneralParams = (SAPbobsCOM.GeneralDataParams)oService.GetDataInterface(SAPbobsCOM.GeneralServiceDataInterfaces.gsGeneralDataParams);
                switch (uDOType)
                {
                    case SAPbobsCOM.BoUDOObjType.boud_MasterData:
                        oGeneralParams.SetProperty("Code", masterCode.Trim());
                        break;
                    case BoUDOObjType.boud_Document:
                        oGeneralParams.SetProperty("DocEntry", masterCode.Trim());
                        break;
                }

                oCabecera = oService.GetByParams(oGeneralParams);
                olinea = GetLineaUDO(oCabecera.Child(uDOChild), lineId);
                if (olinea != null)
                {
                    foreach (FieldSAP value in listvalues)
                        olinea.SetProperty(value.FieldName,value.Value);
                    oService.Update(oCabecera);
                }
                else
                    return false;
            }
            
            catch (Exception e)
            {
                b = 1;
               
            }

            oCompanyService = null;
            oGeneralParams = null;
            oService = null;
            oCabecera = null;
            olinea = null;
            GC.Collect();


            return true;

        }
      
    }
}