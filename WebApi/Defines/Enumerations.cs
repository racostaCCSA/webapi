﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Reflection;
using System.ComponentModel;

namespace WebApi.Defines
{


        public enum StatusDelivery { None, Pendiente, Abierta, Cerrada }

        public enum StatusPickUp
        {
            None,
            [DescriptionAttribute("Pendiente")] Pendiente,
            [DescriptionAttribute("En Transito")] Transito,
            [DescriptionAttribute("Conteo/Limpieza")] Conteo,
            [DescriptionAttribute("Cerrada")] Cerrada
        }

        public enum OrderType
        {
            [DescriptionAttribute("Preparación")] Delivery = 1,
            [DescriptionAttribute("Recogida")] PickUp = 2
        }

        public enum JobType
        {
            [DescriptionAttribute("Textil")] Textil=1,
            [DescriptionAttribute("Transporte")] Transporte = 2,
            [DescriptionAttribute("Conteo")] Conteo = 3,
            [DescriptionAttribute("Limpieza")] Limpieza = 4,
            [DescriptionAttribute("Recogida")] Recogida = 5

        }
    public static class EnumF
    {

        public static string GetDescriptionAttribute(object enumvalue)
        {
            FieldInfo fi = enumvalue.GetType().GetField(enumvalue.ToString());

            if (null != fi)
            {
                object[] attrs = fi.GetCustomAttributes(typeof(DescriptionAttribute), true);
                if (attrs != null && attrs.Length > 0)
                    return ((DescriptionAttribute)attrs[0]).Description;
            }
            return enumvalue.ToString();
        }
        
    }

}
